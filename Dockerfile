# Build Geth in a stock Go builder container
FROM golang:1.15-alpine as builder

RUN apk add --no-cache make gcc musl-dev linux-headers git

ADD . /go-egem
RUN cd /go-egem && make geth

# Pull Geth into a second stage deploy alpine container
FROM alpine:latest

RUN apk add --no-cache ca-certificates
COPY --from=builder /go-egem/build/bin/egem /usr/local/bin/

EXPOSE 8895 8896 30666 30666/udp
ENTRYPOINT ["egem"]
