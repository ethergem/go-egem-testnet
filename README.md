## Go EGEM

Official golang implementation of the EGEM protocol.
[![Discord](https://img.shields.io/badge/discord-join%20chat-blue.svg)](https://discord.egem.io)

Binary archives are published at https://gitlab.com/ethergem/egem-binaries

## Building the source

Update the server and download required.
```
apt-get update
apt-get upgrade -y
apt-get install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev
```
Download & Install Go-lang
```
wget https://golang.org/dl/go1.15.13.linux-amd64.tar.gz
```
Unpack & Move
```
tar -C /usr/local -xzf go1.15.13.linux-amd64.tar.gz
```
Add go to ~/.profile.
```
nano ~/.profile
```
Enter at very bottom of file.
```
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin
```
Refresh profile
```
source ~/.profile
```

Make sure go-egem folder has proper permissions to build.
```
chmod 777 go-egem
```
Then enter into folder and run the commands to make the files needed.

Once the dependencies are installed, run
```
make egem
```
or, to build the full suite of utilities:
```
make all
```
## Executables

The go-egem project comes with several wrappers/executables found in the `cmd` directory.

| Command    | Description |
|:----------:|-------------|
| **`egem`** | Our main EGEM CLI client. It is the entry point into the EGEM network. |
| **`stats`** | Quarrynode JSON output for usage with our system. |

## Custom Commands

### Fast node on the main EGEM network

By far the most common scenario is people wanting to simply interact with the Ethereum network:
create accounts; transfer funds; deploy and interact with contracts. For this particular use-case
the user doesn't care about years-old historical data, so we can fast-sync quickly to the current
state of the network. To do so:

```
egem --syncmode fast
```

This command will:

 * Start egem in fast sync mode (default, can be changed with the `--syncmode` flag), causing it to
   download more data in exchange for avoiding processing the entire history of the EGEM network,
   which is very CPU intensive.

### Configuration

List egem flags/command.

```
egem help
```

As an alternative to passing the numerous flags to the `egem` binary, you can also pass a configuration file via:

```
egem --config /path/to/your_config.toml
```

To get an idea how the file should look like you can use the dumpconfig subcommand to export your existing configuration:


```
$ egem --your-favourite-flags dumpconfig > filetodump.toml
```

Do not forget `--rpcaddr 0.0.0.0`, if you want to access RPC from other containers and/or hosts. By default, `egem` binds to the local interface and RPC endpoints is not accessible from the outside.

## License

The go-egem library (i.e. all code outside of the `cmd` directory) is licensed under the
[GNU Lesser General Public License v3.0](https://www.gnu.org/licenses/lgpl-3.0.en.html), also
included in our repository in the `COPYING.LESSER` file.

The go-egem binaries (i.e. all code inside of the `cmd` directory) is licensed under the
[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html), also included
in our repository in the `COPYING` file.
